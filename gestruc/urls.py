from django.conf import settings
from django.urls import include, path, re_path
from django.conf.urls import include, url
from . import views
# from gestruc.views import ColoniesUpdate
# from django.views.generic import TemplateView, ListView
import debug_toolbar

app_name = 'gestruc'

urlpatterns = [
    path("", views.index, name="index"),
    path('__debug__/', include(debug_toolbar.urls)),
    path("ruchers/", views.ruchers_list, name="ruchers_list"),
    path("miellerie/", views.miellerie, name="miellerie"),
    path("interventions/", views.interventions, name="interventions"),
    path("validation/", views.validation, name="validation"),
    path("ruchers/<int:pk>/", views.rucher_detail, name="rucher_detail"),
    path("ruchers/add/", views.RuchersCreateView.as_view(), name="rucher_add"),
    path("ruchers/<int:pk>/edit/", views.rucher_update, name="rucher_update"),
    path("ruchers/<int:pk>/del/", views.rucher_delete, name="rucher_delete"),
    path("ruches/<int:pk>/", views.ruche_detail, name="ruche_detail"),
    path("ruches/<int:pk>/edit/", views.RuchesUpdate.as_view(), name='ruche_edit'),
    path("ruches/add/", views.RuchesCreate.as_view(), name='ruche_add'),
    path("colonies/", views.ColoniesList.as_view(), name="colonies"),
    path("colonies/add/", views.ColoniesCreateView.as_view(), name="colonie_add"),
    path("colonies/<int:pk>/", views.colonie_detail, name="colonies_detail"),
    path("colonies/<int:pk>/edit", views.ColoniesUpdate.as_view(), name='colonies_edit'),
    path("ruches/", views.ruches_list, name="ruches"),
    path("colonies/add", views.ColoniesCreateView.as_view(), name="colonie_add"),
    path("visites/", views.visites_list, name="visites"),
    path("visites/<int:pk>/", views.visite_detail, name="visite_detail"),
    path("visites/add/", views.VisiteCreateView.as_view(), name="visite_add"),
    path(
        "visites/ajax/load-affectation/",
        views.load_affectation,
        name="ajax_load_affectation",
    ),
    path("sites/", views.SiteListView.as_view(), name="sites_list"),
    path("sites/add/", views.SiteCreateView.as_view(), name="site_add"),
    path("sites/<int:pk>/", views.SiteUpdateView.as_view(), name="site_update"),
    path("sites/ajax/load-communes/", views.load_communes, name="ajax_load_communes"),
    path("ruchers/ajax/load-communes/", views.load_communes, name="ajax_load_communes"),
    path("partenaires/ajax/load-codepostal/", views.load_codepostal, name="ajax_load_codepostal"),
    path("affectations/", views.aff_list, name="aff_list"),
    path("affectations/all/", views.aff_hist, name="aff_hist"),
    path(
        "affectations/<int:pk>/",
        views.aff_ruruchcol_detail,
        name="aff_ruruchcol_detail",
    ),
    path("affectations/add/", views.aff_ruruchcol_create, name="aff_ruruchcol_add"),
    path("affectations/<int:pk>/del/", views.aff_ruruchcol_delete, name="aff_del"),
    path("affectations/<int:pk>/fin/", views.aff_ruruchcol_fin, name="aff_fin"),
    path("interventions/", views.interventions, name="interventions"),
    path(
        "interventions/ajax/load-affectation/",
        views.load_affectation,
        name="ajax_load_affectation",
    ),
    path("interventions/add/", views.IntervCreateView.as_view(), name="interv_add"),
    path(
        "interventions/nourrissement/list",
        views.nour_list,
        name="nour_list"
    ),
    path(
        "interventions/nourrissement/add/",
        views.NourrissementCreate.as_view(),
        name="nour_add",
    ),
    path(
        "interventions/nourrissement/<int:pk>/",
        views.nour_detail,
        name="nour_detail",
    ),
    path("interventions/essaim", views.essaim, name="essaim"),
    path("interventions/mvt_cadres", views.mvt_cadres, name="mvt_cadres"),
    path("interventions/varroa", views.varroa, name="varroa"),
    path("interventions/recolte", views.recolte, name="recolte"),
    path("partenaires/", views.PartenaireListView.as_view(), name="partenaires_list"),
    path("partenaires/add/", views.PartenaireCreateView.as_view(), name="partenaire_add"),

]
