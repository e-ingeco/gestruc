# Generated by Django 3.1.6 on 2021-10-27 21:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gestruc', '0002_auto_20211026_2028'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ruchers',
            name='dept',
            field=models.CharField(blank=True, max_length=2, null=True),
        ),
    ]
