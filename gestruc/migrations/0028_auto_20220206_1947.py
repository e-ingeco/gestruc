# Generated by Django 3.2.11 on 2022-02-06 18:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gestruc', '0027_auto_20220206_1813'),
    ]

    operations = [
        migrations.CreateModel(
            name='Note_couvain',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('note', models.CharField(max_length=4)),
                ('description', models.CharField(blank=True, max_length=50, null=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='visites',
            name='couv_ferme',
        ),
        migrations.RemoveField(
            model_name='visites',
            name='couv_ouvert',
        ),
        migrations.RemoveField(
            model_name='visites',
            name='type_visite',
        ),
        migrations.AlterField(
            model_name='visites',
            name='affectation',
            field=models.ManyToManyField(to='gestruc.Ruruchcol'),
        ),
    ]
