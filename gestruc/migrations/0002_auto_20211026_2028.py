# Generated by Django 3.1.6 on 2021-10-26 18:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gestruc', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ruchers',
            name='code_rucher',
            field=models.CharField(blank=True, default='CODE DU RUCHER', max_length=20, null=True),
        ),
        migrations.AlterField(
            model_name='ruchers',
            name='dept',
            field=models.CharField(blank=True, default='72', max_length=2),
        ),
        migrations.AlterField(
            model_name='ruchers',
            name='lieudit',
            field=models.CharField(blank=True, default='NOM DU LIEU-DIT', max_length=50),
        ),
        migrations.AlterField(
            model_name='ruchers',
            name='nom_rucher',
            field=models.CharField(blank=True, default='NOM DU RUCHER', max_length=25),
        ),
    ]
