# Generated by Django 3.1.6 on 2022-01-01 17:18

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('gestruc', '0020_auto_20220101_1800'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ruruchcol',
            name='colonie',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='colonie', to='gestruc.colonies'),
        ),
    ]
