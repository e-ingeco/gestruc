from django import forms
import django.forms.utils
import django.forms.widgets
from django.contrib.gis import forms
from django.forms.utils import ErrorList
from bootstrap_datepicker_plus import (DatePickerInput,
    TimePickerInput,
    DateTimePickerInput,
    MonthPickerInput,
    YearPickerInput
    )

from .models import (
    Site,
    Departement,
    Commune,
    Ruchers,
    Ruches,
    Colonies,
    Visites,
    Ruruchcol,
    Interv,
    Nourrissement,
    Partenaire,
    Operateur
)


class ParagraphErrorList(ErrorList):
    def __str__(self):
        return self.as_divs()

    def as_divs(self):
        if not self:
            return ""
        return '<div class="errorlist">%s</div>' % "".join(
            ['<p class="small error">%s</p>' % e for e in self]
        )


class SiteForm(forms.ModelForm):
    class Meta:
        model = Site
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["commune"].queryset = Commune.objects.none()

        if "departement" in self.data:
            try:
                departement_id = int(self.data.get("departement"))
                self.fields["commune"].queryset = Commune.objects.filter(
                    insee_dep=departement_id
                ).order_by("nom_com_m")
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.pk:
            self.fields[
                "commune"
            ].queryset = self.instance.departement.commune_set.order_by("nom_com_m")


class RuchersForm(forms.ModelForm):
    class Meta:
       model = Ruchers
#       exclude = ("geom",)
       fields = "__all__"

       geom = forms.PointField(widget=
               forms.OSMWidget(attrs={
                'map_width': 800,
                'map_height': 500,
                'map.srid': 2154,
                'map.template_name': "openlayers-osm.html",
                }))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["commune"].queryset = Commune.objects.none()

        if "departement" in self.data:
            try:
                departement_id = int(self.data.get("departement"))
                self.fields["commune"].queryset = Commune.objects.filter(
                    insee_dep=departement_id
                ).order_by("nom_com_m")
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.pk:
            self.fields[
                "commune"
            ].queryset = self.instance.departement.commune_set.order_by("nom_com_m")

class PartenaireForm(forms.ModelForm):
    class Meta:
        model = Partenaire
        fields = "__all__"

#      def __init__(self, *args, **kwargs):
#         super().__init__(*args, **kwargs)
#         self.fields["comcp"].queryset = Codpostal.objects.none()
# #
# #    def __init__(self, *args, **kwargs):
#        super().__init__(*args, **kwargs)
#        self.fields["comcp"].queryset = Codpostal.objects.none()
#
#        if "departement" in self.data:
#            try:
#                departement_id = int(self.data.get("departement"))
#                self.fields["commune"].queryset = Commune.objects.filter(
#                    insee_dep=departement_id
#                ).order_by("nom_com_m")
#            except (ValueError, TypeError):
#                pass  # invalid input from the client; ignore and fallback to empty City queryset
#        elif self.instance.pk:
#            self.fields[
#                "commune"
#            ].queryset = self.instance.departement.commune_set.order_by("nom_com_m")
#

class RucheForm(forms.ModelForm):
    class Meta:
        model = Ruches
        fields = "__all__"


class ColoniesForm(forms.ModelForm):
    class Meta:
        model = Colonies
        fields = '__all__'

class ColoniesDetailForm(forms.ModelForm):
    class Meta:
        model = Colonies
        fields = "__all__"


class VisiteDetailForm(forms.ModelForm):
    class Meta:
        model = Visites
        fields = "__all__"


class VisiteForm(forms.ModelForm):
    class Meta:
        model = Visites
        fields = "__all__"

        operateur = forms.MultipleChoiceField(
            required=True,
            # widget=forms.CheckboxSelectMultiple,
            choices=Operateur.objects.all(),
        )

        widgets = {
            'datevis': DatePickerInput(
                options={
                    "format": "DD/MM/YYYY",  # moment date-time format
                    "showClose": True,
                    "showClear": True,
                    "showTodayButton": True,
                }
            ),
            'operateur': forms.CheckboxSelectMultiple,
            'mention_couvain': forms.CheckboxSelectMultiple,
            'dateprog': DatePickerInput(
                options={
                    "format": "DD/MM/YYYY",  # moment date-time format
                    "showClose": True,
                    "showClear": True,
                    "showTodayButton": True,
                }
            ),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["affectation"].queryset = Ruruchcol.objects.none()
        self.fields["rucher"].queryset = Ruchers.objects.filter(statut_rucher='True')
        if "rucher" in self.data:
            try:
                rucher_id = int(self.data.get("rucher"))
                self.fields["affectation"].queryset = Ruruchcol.objects.filter(
                    rucher=rucher_id
                )  # .order_by('rucher')
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.pk:
            self.fields[
                "affectation"
            ].queryset = self.instance.rucher.affectation_set.order_by("ruche")


class AffruruchcolForm(forms.ModelForm):
    class Meta:
        model = Ruruchcol
        fields = "__all__"

        ruchvid = forms.BooleanField()
        colonie = forms.ChoiceField(
            widget=forms.Select,
            choices=Colonies
        )
        widgets = {
            'debut': DatePickerInput(format='%d/%m/%Y'), # python date-time format
            'fin': DatePickerInput(
                options={
                    "format": "DD/MM/YYYY", # moment date-time format
                    "showClose": True,
                    "showClear": True,
                    "showTodayButton": True,
                }
            ),
        }

    def clean_fin(self):
        ddebut = self.cleaned_data["debut"]
        dfin = self.cleaned_data["fin"]
        if dfin != None and ddebut > dfin:
            raise forms.ValidationError(
                "la date de fin doit être postérieure à la date de début"
            )
            pass

    def clean_ruche(self):
        clean_ruch = self.cleaned_data["ruche"]
        rruch = Ruruchcol.objects.filter(fin__isnull=True).filter(ruche_id=clean_ruch)
        if rruch.count() != 0:
            raise forms.ValidationError("la ruche est déjà affectée ")
        return clean_ruch

    def clean_colonie(self):
        clean_col = self.cleaned_data["colonie"]
        ruchvid = self.cleaned_data["ruche_vide"]
        rcol = Ruruchcol.objects.filter(fin__isnull=True).filter(colonie_id=clean_col)
        if ruchvid != True and col.count() != 0 :
             raise forms.ValidationError("la colonie est déjà affectée : " )
        return clean_col
        return ruchvid

    def __init__(self, *args, **kwargs):
        super(AffruruchcolForm, self).__init__(*args, **kwargs)
        self.fields['rucher'].queryset = Ruchers.objects.filter(statut_rucher='True')
        rucher = forms.ChoiceField(
            widget=forms.Select,
            choices=Ruchers.objects.order_by("nom_rucher")
        )






class IntervForm(forms.ModelForm):
    class Meta:
        model = Interv
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["affectation"].queryset = Ruruchcol.objects.none()

        if "rucher" in self.data:
            try:
                rucher_id = int(self.data.get("rucher"))
                self.fields["affectation"].queryset = Ruruchcol.objects.filter(
                    rucher=rucher_id
                ).order_by("rucher")
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.pk:
            self.fields[
                "affectation"
            ].queryset = self.instance.rucher.affectation_set.order_by("ruche")

            widgets = {
            'datevis': DatePickerInput(
                options={
                    "format": "DD/MM/YYYY",  # moment date-time format
                    "showClose": True,
                    "showClear": True,
                    "showTodayButton": True,
                }
            ),
            'operateur': forms.CheckboxSelectMultiple,
            'mention_couvain': forms.CheckboxSelectMultiple,
            'dateprog': DatePickerInput(
                options={
                    "format": "DD/MM/YYYY",  # moment date-time format
                    "showClose": True,
                    "showClear": True,
                    "showTodayButton": True,
                }
            ),
        }


class NourrissementForm(forms.ModelForm):
    class Meta:
        model = Nourrissement
        fields = "__all__"
        # affectation = forms.MultipleChoiceField(choices=Nourrissement.affectation, widget=forms.CheckboxSelectMultiple())

        widgets = {
            'datenourr': DatePickerInput(
                options={
                    "format": "DD/MM/YYYY",  # moment date-time format
                    "showClose": True,
                    "showClear": True,
                    "showTodayButton": True,
                }
            ),
            'operateur': forms.CheckboxSelectMultiple,
        }
        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.fields["affectation"].queryset = Ruruchcol.objects.none()

            if "rucher" in self.data:
                try:
                    rucher_id = int(self.data.get("rucher"))
                    self.fields["affectation"].queryset = Ruruchcol.objects.filter(
                        rucher=rucher_id
                    ).order_by("rucher")
                except (ValueError, TypeError):
                    pass  # invalid input from the client; ignore and fallback to empty City queryset
            elif self.instance.pk:
                self.fields[
                    "affectation"
                ].queryset = self.instance.rucher.affectation_set.order_by("ruche")
