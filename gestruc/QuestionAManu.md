# Questions à Manu

## Pb ssh Keys pour commit de py charm to Gitlab
Les clés publiques sont bien enregistrée sur Gitlab mais continue de demander login+mp pour connecxion en https

## montage sshfs pour coder sur le serveur...
Je code directement sur le serveur en faisant un montage local en sshfs sur un dossier dans mon home
PyCharm me met un message :
"External file changes sync may be slow: PyCharm cannot receive filesystem event notifications for the project. Is it on a network drive?"
Quand je galere sur des erreurs, je me demande parfois si l'actualisation se fait correctement, ... même si
les tests montrent que non... (je pb reste entre le clavier et la chaise...)

## j'ai un beta testeur..."
le pb est qu'il faut que je rende mon appli accessible...
Je peux le rendre accessible, mais la question que je me pose est la suivante :
Faut-il créer une base de donnée par user pour separer les données ou bien rajouter un couche dans mon modéle de donnée me permettant de
gerer des "dossiers de gestion de ruche" différents?

# Questions techniques :
## widget carte/ positionnement rucher
J'ai un pb d'affichage de la carte dans mon formulaire de creation de rucher...
Je bugue sur le jacvascript... J'ai l'encadrement et les widgets de gestion de la carte mais pas le fond cartographique...

## liste des ruchers avec nombre de ruches présentes
Mon modele est articulé autour d'une table Ruruchcol qui enregistre les affectations
Rucher-Ruche-Colonie
chaque enregistrement comporte une date début (obligatoire), la date de fin n'est pas null
, elle est donc remplie si l'affectation est "terminée".
C'est la formulation informatique d'une fait que nous avons une colonie dans une ruche, sur un rucher...
Ce qui permet de visiter des ruches ( ex N° 43), mais l'historique des interventions qui nous intéresse est en fait sur la colonie...
Cette structure permet cela.
En revanche cela complexifie les requettes ...

## Génération d'un champ à partir de deux autres champs dans formulaire :
Sans doute avec jquery...
Je veux générer automatiquement un Identifiant de visite a partir du code rucher et de la date du jour

## faire apparaitre un menu déroulant si une case est cochée ou décochée...
Js et /ou Jquery... ma solution ne fonctionne pas!
