# Gestapi

> Hive management for professionals

## Getting started

- Install required system packages:
  - postgis
  - postgresql server
- Install dependencies with [poetry](https://python-poetry.org).
  ```shell
  # Install dependencies
  poetry install
  # Use virtualenv (Ctrl+D to quit)
  poetry shell
  ```
- Define environment variables:
  ```shell
  cp .env.sample .env
  vim .env
  ```
- Run migrations
  ```shell
  ./manage.py migrate
  ```
- Start server
  ```shell
  ./manage.py runserver
  ```

## Create superuser

As we don't have seeds for now, you have to create a superuser from console:

```shell
./manage.py shell
```

then:

```python
from django.contrib.auth.models import User
User.objects.create_superuser(username='admin', email='admin@example.com', password='password')
```

Visit http://localhost:8000/admin and login with `admin`/`password`.
